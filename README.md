# Unity

This project is an example of unit testing using the Unity
framework.

-------------------------------------------------------------------------------

Unity is a C unit testing framework written in pure ANSI C
for embedded projects. It is extremely portable running on
everything from 8-bit to 64-bit processors but it is
extremely simple to use. It can also run tests both natively or
on a simulator for the target.

## Build and Test

To build and test the project simply run:

    $ make

## Project Structure

-   The `unity` directory

> Contains the three files that make up
the unit test framework. If you ever forget what types of
`ASSERT` functions are available you can always open up
`unity.c` and scroll down to about line 400 and see all
of the available functions for unit testing.

-   The `inc` directory

> In the `inc` directory is the header file `MK20DX256.h` which is
for that model processor in the K20 Sub-Family of processors
by Freescale. See the [K20 Sub-Family Reference Manual](https://www.pjrc.com/teensy/K20P64M72SF1RM.pdf)
for detailed information on the ARM Cortex-M4 Core.

-   The `src` directory

> Contains only the `GPIO.c` and `GPIO.h` files,
these are files for providing an interface to the General Purpose
Input Output pins on the processor. 

-   The `test` directory

> Contains the tests for these the modules, these are by convention
named in the form `test_moduleName` so the correct test file can
easily be found.
