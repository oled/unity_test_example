#include "unity.h"
#include "GPIO.h"
#include "MK20DX256.h"


/**********************************************************************
 * Set pin as out
 */

void test_SetPinAsOutput_should_ConfigurePinDirection(void)
{
    PORTC.PDDR = 0;

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsOutput(0));
    TEST_ASSERT_EQUAL_HEX32(BIT_TO_MASK(0), PORTC.PDDR);

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsOutput(22));
    TEST_ASSERT_EQUAL_HEX32(BIT_TO_MASK(0) | BIT_TO_MASK(22), PORTC.PDDR);

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsOutput(31));
    TEST_ASSERT_EQUAL_HEX32(BIT_TO_MASK(0) | BIT_TO_MASK(22) | BIT_TO_MASK(31),
                            PORTC.PDDR);
}

void test_SetPinAsOutput_should_NotUpdateDirection_when_PinIsNotValid(void)
{
    PORTC.PDDR = 0;

    TEST_ASSERT_NOT_EQUAL(GPIO_OK, GPIO_SetPinAsOutput(32));
    TEST_ASSERT_EQUAL_HEX32(0, PORTC.PDDR);
}


/**********************************************************************
 * Set pin as input
 */

void test_SetPinAsInput_should_ConfigurePinDirection(void)
{
    PORTC.PDDR = 0xFFFFFFFF;

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsInput(0));
    TEST_ASSERT_EQUAL_HEX32(~(BIT_TO_MASK(0)), PORTC.PDDR);

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsInput(16));
    TEST_ASSERT_EQUAL_HEX32(~(BIT_TO_MASK(0) | BIT_TO_MASK(16)), PORTC.PDDR);

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsInput(31));
    TEST_ASSERT_EQUAL_HEX32(~(BIT_TO_MASK(0) | BIT_TO_MASK(16) | BIT_TO_MASK(31)),
                            PORTC.PDDR);
}


void test_SetPinAsInput_should_NotUpdateDirection_when_PinIsNotValid(void)
{
    PORTC.PDDR = 0xFFFFFFFF;

    TEST_ASSERT_NOT_EQUAL(GPIO_OK, GPIO_SetPinAsInput(32));
    TEST_ASSERT_EQUAL_HEX32(0xFFFFFFFF, PORTC.PDDR);
}


/**********************************************************************
 * Set pin
 */

void test_SetPin_should_SetOutputHigh(void)
{
    PORTC.PSOR = 0;
    PORTC.PCOR = 0;

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPin(0));
    TEST_ASSERT_EQUAL_HEX32(BIT_TO_MASK(0), PORTC.PSOR);
    TEST_ASSERT_EQUAL_HEX32(0, PORTC.PCOR);

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPin(31));
    TEST_ASSERT_EQUAL_HEX32(BIT_TO_MASK(31), PORTC.PSOR);
    TEST_ASSERT_EQUAL_HEX32(0, PORTC.PCOR);
}


void test_SetPin_should_ForceToOutput_when_ConfiguredAsInput(void)
{
    PORTC.PSOR = 0;
    PORTC.PCOR = 0;
    PORTC.PDDR = 0x50000000;

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPin(0));
    TEST_ASSERT_EQUAL_HEX32(BIT_TO_MASK(0), PORTC.PSOR);
    TEST_ASSERT_EQUAL_HEX32(0, PORTC.PCOR);
    TEST_ASSERT_EQUAL_HEX32(0x50000001, PORTC.PDDR);
}


void test_SetPin_should_NotSetOutputs_when_PinIsNotValid(void)
{
    PORTC.PSOR = 0;
    PORTC.PCOR = 0;
    PORTC.PDDR = 0;

    TEST_ASSERT_NOT_EQUAL(GPIO_OK, GPIO_SetPinAsOutput(32));
    TEST_ASSERT_EQUAL_HEX32(0, PORTC.PSOR);
    TEST_ASSERT_EQUAL_HEX32(0, PORTC.PCOR);
    TEST_ASSERT_EQUAL_HEX32(0, PORTC.PDDR);
}


/**********************************************************************
 * Clear pin
 */


void test_ClearPin_should_SetOutputLow(void)
{
    PORTC.PSOR = 0xFFFFFFFF;
    PORTC.PCOR = 0xFFFFFFFF;

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_ClearPin(0));
    TEST_ASSERT_EQUAL_HEX32(~(BIT_TO_MASK(0)), PORTC.PCOR);
    TEST_ASSERT_EQUAL_HEX32(0xFFFFFFFF, PORTC.PSOR);

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_ClearPin(31));
    TEST_ASSERT_EQUAL_HEX32(~(BIT_TO_MASK(31)), PORTC.PCOR);
    TEST_ASSERT_EQUAL_HEX32(0xFFFFFFFF, PORTC.PSOR);
}


void test_ClearPin_should_ForceToOutput_when_ConfiguredAsInput(void)
{
    PORTC.PSOR = 0xFFFFFFFF;
    PORTC.PCOR = 0xFFFFFFFF;
    PORTC.PDDR = 0x50000000;

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_ClearPin(0));
    TEST_ASSERT_EQUAL_HEX32(~(BIT_TO_MASK(0)), PORTC.PCOR);
    TEST_ASSERT_EQUAL_HEX32(0xFFFFFFFF, PORTC.PSOR);
    TEST_ASSERT_EQUAL_HEX32(0x50000001, PORTC.PDDR);
}


void test_ClearPin_should_NotSetOutputs_when_PinIsNotValid(void)
{
    PORTC.PSOR = 0xFFFFFFFF;
    PORTC.PCOR = 0xFFFFFFFF;
    PORTC.PDDR = 0xFFFFFFFF;

    TEST_ASSERT_NOT_EQUAL(GPIO_OK, GPIO_SetPinAsOutput(32));
    TEST_ASSERT_EQUAL_HEX32(0xFFFFFFFF, PORTC.PSOR);
    TEST_ASSERT_EQUAL_HEX32(0xFFFFFFFF, PORTC.PCOR);
    TEST_ASSERT_EQUAL_HEX32(0xFFFFFFFF, PORTC.PDDR);
}


/**********************************************************************
 * Runner
 */

int main(void) {
    UNITY_BEGIN();

    // Set pin as output
    RUN_TEST(test_SetPinAsOutput_should_ConfigurePinDirection);
    RUN_TEST(test_SetPinAsOutput_should_NotUpdateDirection_when_PinIsNotValid);

    // Set pin as input
    RUN_TEST(test_SetPinAsInput_should_ConfigurePinDirection);
    RUN_TEST(test_SetPinAsInput_should_NotUpdateDirection_when_PinIsNotValid);

    // Set pin
    RUN_TEST(test_SetPin_should_SetOutputHigh);
    RUN_TEST(test_SetPin_should_ForceToOutput_when_ConfiguredAsInput);
    RUN_TEST(test_SetPin_should_NotSetOutputs_when_PinIsNotValid);

    // Clear pin
    RUN_TEST(test_ClearPin_should_SetOutputLow);
    RUN_TEST(test_ClearPin_should_ForceToOutput_when_ConfiguredAsInput);
    RUN_TEST(test_ClearPin_should_NotSetOutputs_when_PinIsNotValid);

    return UNITY_END();
}
